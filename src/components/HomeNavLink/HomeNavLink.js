import React from 'react';
import {NavLink} from "react-router-dom";
import {CATEGORIES} from "../../constants";

import './HomeNavLink.css';

const HomeNavLink = () => {
    return (
        <nav className="HomeNav">
            <NavLink className="HomeNavLink"  activeClassName="Active" to="/" exact>All quotes</NavLink>
            {Object.keys(CATEGORIES).map(categoryId => (
                <NavLink className="HomeNavLink"   activeClassName="Active" key={categoryId} to={"/quotes/" + categoryId}
                         exact>{CATEGORIES[categoryId]}</NavLink>
            ))}
        </nav>
    );
};

export default HomeNavLink;