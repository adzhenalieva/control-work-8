import React from 'react';
import './QuoteList.css';

const QuoteList = props => {
    return (
        <div className="QuoteList">
            <p>{props.text}</p>
            <p className="Author">{props.author}</p>
            <button className="Button" onClick={props.goToEdit}>Edit</button>
            <button className="Button" onClick={props.delete}>Delete</button>
        </div>
    );
};

export default QuoteList;