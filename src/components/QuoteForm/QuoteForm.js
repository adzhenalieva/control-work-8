import React, {Component} from 'react';
import {CATEGORIES} from "../../constants";

import './QuoteForm.css';

class QuoteForm extends Component {

    constructor(props) {
        super(props);
        if (props.quote) {
            this.state = {...props.quote};
        } else {
            this.state = {
                text: '',
                author: '',
                category: Object.keys(CATEGORIES)[0]
            };
        }
    }

    valueChanged = event => {
        let name = event.target.name;
        this.setState({[name]: event.target.value})

    };

    submit = event => {
        event.preventDefault();
        this.props.onSubmit({...this.state});
    };

    render() {
        return (
            <form className="QuoteForm" onSubmit={this.submit}>
                <select name="category"
                        onChange={this.valueChanged}
                        value={this.state.category}>
                    {Object.keys(CATEGORIES).map(categoryID => (
                            <option key={categoryID} value={categoryID}>{CATEGORIES[categoryID]}</option>
                        )
                    )}
                </select>
                <textarea name="text"
                          onChange={this.valueChanged}
                          value={this.state.text}
                />
                <input type="text" name="author"
                       onChange={this.valueChanged}
                       value={this.state.author}
                />
                <button type="submit">Save</button>
            </form>
        );
    }
}

export default QuoteForm;