import React, {Component} from 'react';
import {NavLink} from 'react-router-dom';

import './MainNav.css';


class Nav extends Component {

    render() {
        return (
            <nav className="MainNav">
                <h3 className="Logo">Quotes</h3>
                <NavLink className="NavLink"   activeClassName="Active" to="/">Home</NavLink>
                <NavLink className="NavLink"  to="/quotes/add">Add</NavLink>
            </nav>
        );
    }
}

export default Nav;