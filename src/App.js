import React, {Component} from 'react';
import {BrowserRouter, Switch, Route} from "react-router-dom";

import HomePage from "./containers/HomePage/HomePage";
import QuoteAdd from "./containers/QuoteAdd/QouteAdd";
import QuoteEdit from "./containers/QuoteEdit/QuoteEdit";

import './App.css';



class App extends Component {
    render() {
        return (
            <div className="App">
                    <BrowserRouter>
                        <Switch>
                            <Route path="/" exact component={HomePage}/>
                            <Route path="/quotes/add" exact component={QuoteAdd}/>
                            <Route path="/quotes/:categoryId/edit" component={QuoteEdit}/>
                            <Route path="/quotes/:categoryId" component={HomePage}/>
                        </Switch>
                    </BrowserRouter>
            </div>
        );
    }
}

export default App;
