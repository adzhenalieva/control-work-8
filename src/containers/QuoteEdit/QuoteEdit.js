import React, {Component} from 'react';
import axios from '../../axios-quotes';

import QuoteForm from "../../components/QuoteForm/QuoteForm";
import Spinner from "../../components/UI/Spinner/Spinner";

import './QuoteEdit.css';


class QuoteEdit extends Component {

    state = {
        quote: null
    };

    getUrl = () => {
        const id = this.props.match.params.categoryId;
        return 'quotes/' + id + '.json'
    };

    componentDidMount() {
        axios.get(this.getUrl()).then(response => {
            this.setState({quote: response.data});

        })
    }

    edit = quote => {
        axios.put(this.getUrl(), quote).then(() => {
            this.props.history.replace('/');
        })
    };

    render() {
        let quote = <QuoteForm onSubmit={this.edit} quote={this.state.quote}/>;
        if (!this.state.quote) {
            quote = <Spinner/>
        }
        return (
            <div className="QuoteEdit">
                <h2>Edit quote</h2>
                {quote}
            </div>
        );
    }
}

export default QuoteEdit;