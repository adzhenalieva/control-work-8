import React, {Component, Fragment} from 'react';
import axios from "../../axios-quotes";

import MainNav from "../../components/MainMav/MainNav";
import Spinner from "../../components/UI/Spinner/Spinner";
import QuoteList from "../../components/QuotesList/QuoteList";
import HomeNavLink from "../../components/HomeNavLink/HomeNavLink";

import './HomePage.css';




class HomePage extends Component {

    state = {
        quotes: null,
        loading: true
    };

    loadQuotes() {
        let url = 'quotes.json';
        const categoryId = this.props.match.params.categoryId;
        if (categoryId) {
            url += `?orderBy="category"&equalTo="${categoryId}"`
        }
        axios.get(url).then(response => {
            const quotes = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });
            this.setState({quotes, loading: false})
        })
    }

    componentDidMount() {
        this.loadQuotes();
    }

    componentDidUpdate(prevProps) {
        if (this.props.match.params.categoryId !== prevProps.match.params.categoryId) {
            this.loadQuotes();
        }
    }

    goToEdit = (id) => {
        this.props.history.push({
            pathname: '/quotes/' + id + '/edit'
        });
    };

    delete = (id) => {
        axios.delete('/quotes/' + id + '.json').then(() => {
            this.loadQuotes();
        })
    };

    render() {
        let quotes = null;
        if (this.state.quotes) {
            quotes = this.state.quotes.map((quote) => (
                <QuoteList key={quote.id}
                           text={quote.text}
                           author={quote.author}
                           goToEdit={() => this.goToEdit(quote.id)}
                           delete={() => this.delete(quote.id)}>
                </QuoteList>
            ))

        }

        return (
            this.state.loading ?
                <Spinner/> :
                <Fragment>
                    <MainNav/>
                    <div className="HomePage">
                        <div className="HomeNavLinks">
                           <HomeNavLink />
                        </div>
                        <div className="Quotes">
                            {quotes}
                        </div>
                    </div>
                </Fragment>
        );
    }
}

export default HomePage;