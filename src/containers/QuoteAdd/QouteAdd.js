import React, {Component} from 'react';
import axios from '../../axios-quotes';

import QuoteForm from "../../components/QuoteForm/QuoteForm";

import './QuoteAdd.css';


class QuoteAdd extends Component {

    addQuote = quote => {
        axios.post('quotes.json', quote).then(() => {
            this.props.history.replace('/');
        })
    };

    render() {
        return (
            <div className="QuoteAdd">
                <h1>Write new quote</h1>
                <QuoteForm onSubmit={this.addQuote}/>
            </div>
        );
    }
}

export default QuoteAdd;